# CUTEst
The Constrained and Unconstrained Testing Environment with safe threads (CUTEst) for optimization software.

This fork includes the updated interface for PETSc/TAO solvers.

### Dependencies
1. [denera/ARCHDefs](https://github.com/denera/ARCHDefs)
2. [SIFDecode](https://github.com/ralna/SIFDecode)

### Installation
1. Pull ARCHDefs, SIFDecode and CUTEst repos to your local machine.
2. Set environment variables `$ARCHDEFS`, `$SIFDECODE` and `$CUTEST` to the directories for each repository.
3. Enter the CUTEst directory and run `$ARCHDEFS/install_optrove`.
4. Follow instructions on screen, making selections appropriate to your architecture and compilers.
5. Set the environment variable `$MYARCH` to the architecture you selected during installation.
6. Pull test problems from [SIF](https://bitbucket.org/optrove/sif/wiki/Home). If you pull the entire repo, it may take a few minutes.
7. Set the environment variable `$MASTSIF` to wherever you have the SIF test problems stored.
8. **Optional:** Pull the [TAOster](https://github.com/denera/TAOster) repository and use the `profile.py` script to run CUTEst problems in bulk with TAO algorithms.

**NOTE:** If you're using CUTEst with PETSc/TAO, you must select the same compilers that PETSc has been built with. If this compiler selection is not available, you can either add it to ARCHDefs as a new architecture definition, or manually modify the configuration during the installation.

[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)
